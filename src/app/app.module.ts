

import { CompetitionComponent } from './pages/competition/competition.component';
import { TestimoniComponent } from './pages/testimoni/testimoni.component';
import { MainComponent } from './pages/main/main.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlickityModule } from 'ngx-flickity';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    TestimoniComponent,
    CompetitionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlickityModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
