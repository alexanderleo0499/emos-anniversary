
import { CompetitionComponent } from './pages/competition/competition.component';
import { TestimoniComponent } from './pages/testimoni/testimoni.component';
import { MainComponent } from './pages/main/main.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'',component:MainComponent, pathMatch:'full'},
  {path:'testimoni',component:TestimoniComponent},
  {path:'competition',component:CompetitionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
