import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  imgSrc:string=''
  date:number =  new Date().getDate()

  flickityOptions={
    autoPlay: 3000,
    pauseAutoPlayOnHover: false
  }

  assets=[
    'agustus-01.jpg',
    'agustus-02.jpg',
    'agustus-03.jpg',
    'agustus-04.jpg',
    'agustus-05.jpg',
    'agustus-06.jpg',
    'agustus-07.jpg',
    'agustus-08.jpg',
    'popup-09.jpg'
  ]
  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
    // this.route.queryParams.subscribe(params=>{

    // })
    const baseSrc = 'assets/images/katalog'
    if(this.date===1){
      this.imgSrc = `${baseSrc}/agustus-01.jpg`
    }
    else if (this.date===2){
      this.imgSrc = `${baseSrc}/agustus-02.jpg`
    }
    else if (this.date===3){
      this.imgSrc = `${baseSrc}/agustus-03.jpg`
    }
    else if (this.date===4){
      this.imgSrc = `${baseSrc}/agustus-04.jpg`
    }
    else if (this.date===5){
      this.imgSrc = `${baseSrc}/agustus-05.jpg`
    }
    else if(this.date===6){
      this.imgSrc = `${baseSrc}/agustus-06.jpg`
    }
    else if(this.date===7){
      this.imgSrc = `${baseSrc}/agustus-07.jpg`
    }
    else if (this.date===8){
      this.imgSrc = `${baseSrc}/agustus-08.jpg`
    }
  }

}
